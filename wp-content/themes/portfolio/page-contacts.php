<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>


<div class="asyncContent homePage" data-page="Home">
    <!-- batas -->
<div class="asyncContent secondaryPage contactPage scrollable" data-page="Contact">
    <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
 ?>
    <div class="contentWrapper" style=" background: url(<?php echo $feat_image; ?>) center center no-repeat fixed #3c3837;">
        <div class="headingContainer">
            <h1 class="dark"><?php the_title() ?></h1>
            <span class="icon-ornament dark"></span>
        </div>

        <section class="secondarySection">
            <?php the_content() ?>
        </section>
    </div>
    <?php

get_footer();
