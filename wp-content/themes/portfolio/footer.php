<?php
/**
* The template for displaying the footer
*
* Contains footer content and the closing of the #main and #page div elements.
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
?>
<footer class="siteFooter" data-ctrl="nav">
    <nav class="secondaryNav">
        <ul class="footerLinkGroup shopLinks">
            <li data-for="Shop">
                <a href="" class="siteBtn outlinedBtn leftIconBtn" data-async>
                    <div><span class="icon icon-lupa"></span>Search</div></a>
                </li>
            </ul>
            <div class="linksGroupWrapper">
                <?php wp_nav_menu( array( 'menu' => 'sub-menu' , 'menu_class' => 'footerLinkGroup secondaryLinks', 'menu_id' => 'sub-menu' ) ); ?>
            </div>
        </nav>
        <div class="disclamerTxt">
            <p><?php $examplePost = get_post(32);
            echo $examplePost->post_content;  ?></p>
            <p>Design by &nbsp; <a href="http://www.north2.net" target="_blank"> north2</a></p>
        </div>
    </footer>
</div>
</div>
</div>
</div> <!-- asyncWrapper -->
</div> <!-- pageWrapper -->

<div class="loadingScreen active initial" data-loading>
<div class="bg" data-bg data-preload></div>
<div class="initialAnim" data-decoration-initial>
<?php $examplePost2 = get_post(29);
echo $examplePost2->post_content;  ?>
<span class="icon-ornament"></span>
</div>
<div class="anim" data-decoration data-preload>
<img src="http://localhost/ey/portfolio/wp-content/themes/portfolio/images/design/gs_loader.png">
</div>
</div>
<div class="darkOverlay" data-lightbox-mask data-ctrl="close-lightbox">
<div class="anim" data-decoration data-preload>
<img src="images/design/gs_loader.png" alt="Loading...">
</div>
</div>
<?php //wp_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/main-body-prod.min.js"></script>
<script>
$(document).ready(function () {
$('html').addClass('production', 0);
App.init({
transitionMicro: 250,
transitionVeryShort: 400,
transitionShort: 600,
transitionMed: 900,
transitionLong: 1200,
lightboxUrl: 'lightbox.html'
});
$('#menu-main-menu li').addClass("pageLinkBox horizontal");
$('#menu-main-menu li a').addClass("pageLinkTitle");
$(".headerSlide:first").addClass("active");
$(".wpb_wrapper").replaceWith(function() { return $(this).contents(); });
$(".wpb_text_column").replaceWith(function() { return $(this).contents(); });
$(".wpb_column").replaceWith(function() { return $(this).contents(); });
$(".vc_row").replaceWith(function() { return $(this).contents(); });
});
</script>
</body>
</html>