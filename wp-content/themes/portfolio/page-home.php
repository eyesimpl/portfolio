<?php
/**
* The template for displaying all pages
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages and that
* other 'pages' on your WordPress site will use a different template.
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header(); ?>
<!-- batas content dinamis -->
<div class="asyncContent homePage" data-page="Home">
    <div class="contentWrapper">
        <!--<div class="fullSpaceBox">
            <section class="fullSpaceSection mainNavSection">
                <div class="bgImageContainer" data-preload data-ctrl="close-news" data-onload="settle-in" data-onload-i="0"></div>
                <div class="containerWider centeredBox dark">
                    <div class="pageLinkBox horizontal" data-linkbox>
                        <div class="pageLinkCtrl" data-onload="settle-in-fade" data-onload-i="0">
                            <a href="story.html" class="pageLinkTitle" data-ctrl="expand" data-async><h2>Our story</h2></a>
                        </div>
                        <div class="pageLinkCopy" data-details>
                            <p class="pageShortText">
                                <a href="story.html" data-async>Are you ready to discover the ultimate secret?</a>
                            </p>
                            <a href="story.html" class="siteBtn greenBtn longBtn" data-async><div><span>View<span class="icon-small-arrow-right icon"></span></span></div></a>
                        </div>
                    </div>
                    <div class="pageLinkBox horizontal" data-linkbox>
                        <div class="pageLinkCtrl" data-onload="settle-in-fade" data-onload-i="0">
                            <a href="#" class="pageLinkTitle" data-ctrl="expand show-subnav"><h2>Products</h2></a>
                        </div>
                        <div class="pageLinkCopy" data-details>
                            <p class="pageShortText">
                                <a href="#" data-ctrl="show-subnav">We stand on the shoulders of our ancestors</a>
                            </p>
                            <a href="#" class="siteBtn greenBtn longBtn" data-ctrl="show-subnav"><div><span>View<span class="icon-small-arrow-right icon"></span></span></div></a>
                        </div>
                    </div>
                    <div class="pageLinkBox horizontal" data-linkbox>
                        <div class="pageLinkCtrl" data-onload="settle-in-fade" data-onload-i="0">
                            <a href="recipes/" class="pageLinkTitle" data-ctrl="expand" data-async><h2>Recipes</h2></a>
                        </div>
                        <div class="pageLinkCopy" data-details>
                            <p class="pageShortText">
                                <a href="recipes/" data-async>Add a little ... secret to your cooking</a>
                            </p>
                            <a href="recipes/" class="siteBtn greenBtn longBtn" data-async><div><span>View<span class="icon-small-arrow-right icon"></span></span></div></a>
                        </div>
                    </div>
                </div>
            </section>
        </div>-->
        <?php remove_filter ('the_content', 'wpautop'); ?>

<?php the_content(); ?>
        <div class="restBox">
            <div class="scrollDownBtn newsButton standardSize">
                <a href="#" data-ctrl="toggle-news">News<span class="icon-medium-arrow-down"></span></a>
            </div>
            <section class="newsListWrapper" data-news>
                <div class="newsListHeader">
                    <h2 class="standardTitle">News</h2>
                    <span class="icon-ornament"></span>
                </div>
                <div class="newsContent" data-news-content>
                    <?php
global $post;
$args = array( 'posts_per_page' => 9,  'category' => 4 );
$myposts = get_posts( $args );
foreach ( $myposts as $post ) : 
  setup_postdata( $post ); ?>
                    <article class="newsItem standardTextChunk">
                        <span class="dateSpan dateSpan"><?php echo get_the_date('d/m/Y'); ?></span>
                        <a href="<?php the_permalink() ?>" data-ctrl="lightbox" data-target="126"><h3><?php the_title() ?></h3></a>
                    </article>
                    <?php endforeach;
wp_reset_postdata(); ?>
                </div>
                <div class="newsItem iconsItem">
                    <a href="#" class="siteBtn greenBtn circle sBtn complex" data-ctrl="news-scroll" data-dir="next">
                        <div>
                            <span class="icon-small-arrow-right icon"></span>
                        </div>
                    </a>
                    <a href="#" class="siteBtn greenBtn circle sBtn complex" data-ctrl="news-scroll" data-dir="prev">
                        <div>
                            <span class="icon-small-arrow-left icon"></span>
                        </div>
                    </a>
                    <a href="<?php site_url() ?>/news" data-async class="siteBtn greenBtn longBtn standardSize">
                        <div>
                            <span>All news<span class="icon-small-arrow-right icon"></span></span>
                        </div>
                    </a>
                </div>
            </section>
            
            <?php
            get_footer();