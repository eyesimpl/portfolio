<?php
/**
* The template for displaying all pages
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages and that
* other 'pages' on your WordPress site will use a different template.
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header(); ?>
<div class="asyncContent newsPage scrollable" data-page="NewsList" data-scrollable>
    <div class="contentWrapper">
        <header class="headerSliderWrapper" data-nav-breakpoint>
            <div class="headerSlider" data-slideshow>
                
                <!-- Instance of featured news article in header -->
                <?php
global $post;
$args = array( 'posts_per_page' => 9,  'category' => 4 );
$myposts = get_posts( $args );
foreach ( $myposts as $post ) : 
  setup_postdata( $post ); ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
 ?>
<div data-slide class="headerSlide">
                    <div class="bgImageContainer" style="background-image: url(<?php echo $feat_image; ?>)"></div>
                    <div data-anim="move-in" data-onload="move-in" data-anim-i="0" data-onload-i="0">
                        <div class="centeredBox dark standardTextChunk">
                            <span class="dateSpan dark"><?php echo get_the_date('d/m/Y'); ?></span>
                            <a href="#" data-ctrl="lightbox" data-target="126">
                                <h2 class="title recipeTitle"><?php the_title() ?></h2>
                            </a>
                        </div>
                    </div>
                </div>
<?php endforeach;
wp_reset_postdata(); ?>
                <div class="navContainer" data-onload="fade-in" data-onload-i="1">
                    <a href="#" class="siteBtn greenBtn circle sBtn complex navBtn navBtn-left" data-ctrl="slideshow" data-dir="-1">
                        <div>
                            <span class="icon-small-arrow-left icon"></span>
                        </div>
                    </a>
                    <a href="#" class="siteBtn greenBtn circle sBtn complex navBtn navBtn-right" data-ctrl="slideshow" data-dir="1">
                        <div>
                            <span class="icon-small-arrow-right icon"></span>
                        </div>
                    </a>
                </div>
            </div>
        </header>
        <div class="restScrollBox">
            <div class="pageMaingHeading">
                <h1 class="standardTitle"><?php the_title() ?></h2>
                <span class="icon-ornament"></span>
            </div>
            <section class="articlesListWrapper">
                <ul>
                    <?php
global $post;
$args = array( 'posts_per_page' => 9,  'category' => 4 );
$myposts = get_posts( $args );
foreach ( $myposts as $post ) : 
  setup_postdata( $post ); ?>
                    <li class="articleLink standardTextChunk">
                        <a href="<?php the_permalink(); ?>" data-ctrl="lightbox" data-target="126">
                            <span class="dateSpan dateSpan"><?php echo get_the_date('d/m/Y'); ?></span>
                            <h2 class="articleSmallTitle"><?php the_title() ?></h2>
                        </a>
                    </li>
                    <?php endforeach;
wp_reset_postdata(); ?>
                </ul>
            </section>
            <?php
            get_footer();