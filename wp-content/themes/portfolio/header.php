<?php
/**
* The Header for our theme
*
* Displays all of the <head> section and everything up till <div id="main">
    *
    * @package WordPress
    * @subpackage Twenty_Fourteen
    * @since Twenty Fourteen 1.0
    */
    ?><!DOCTYPE html>
    <!--[if IE 7]>
    <html class="ie ie7" <?php language_attributes(); ?>>
        <![endif]-->
        <!--[if IE 8]>
        <html class="ie ie8" <?php language_attributes(); ?>>
            <![endif]-->
            <!--[if !(IE 7) & !(IE 8)]><!-->
            <html <?php language_attributes(); ?>>
                <!--<![endif]-->
                <head>
                    <meta charset="<?php bloginfo( 'charset' ); ?>">
                    <meta name="viewport" content="width=device-width">
                    <title><?php wp_title( '|', true, 'right' ); ?></title>
                    <link rel="profile" href="http://gmpg.org/xfn/11">
                    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
                    <link href='https://fonts.googleapis.com/css?family=Arimo&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
                    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />
                    <!--[if lt IE 9]>
                    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
                    <![endif]-->
                    <?php wp_head(); ?>
                    <script src="<?php echo get_template_directory_uri(); ?>/js/main-head-prod.min.js"></script>
                    <!-- Google Analytics -->
                    <script>
                    window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
                    ga('create', 'UA-74997342-1', 'auto');
                    ga('send', 'pageview');
                    </script>
                    <script async src='http://www.google-analytics.com/analytics.js'></script>
                    <!-- End Google Analytics -->
                </head>
                <body <?php body_class(); ?>>
                    <div class="menuTriggerHolder">
                        <a href="<?php site_url() ?>" data-async>
                            <?php get_header_image(); ?>
                            <!--<img class="siteLogo siteLogo-ver" src="<?php echo get_template_directory_uri(); ?>/images/design/grannys-logo-vertical.svg" alt="Granny's Secret Logo">-->
                            <img class="siteLogo siteLogo-ver" src="<?php echo ( get_header_image() ); ?>" alt="">
                        </a>
                        <div class="menuBtnWrapper">
                            <a href="index.html#" class="siteBtn outlinedBtn menuBtn standardSize" data-ctrl="nav">
                                <div class="v4"><span class="menuIcon"></span>Menu</div>
                            </a>
                        </div>
                        <div class="menuBtnWrapper-alter">
                            <a href="index.html#" class="siteBtn outlinedBtn menuBtn standardSize" data-ctrl="nav">
                                <div class="v4"><span class="menuIcon"></span>Menu</div>
                            </a>
                        </div>
                    </div>
                    <nav class="mainNav mainNavSection" data-nav>
                        <div class="bgImageContainer" data-ctrl="nav"></div>
                        <?php wp_nav_menu(array(
                        'menu'            => 'main-menu',
                        'menu_class' => 'containerWider centeredBox dark',
                        'container' => false,
                        'before'          => '<div class="pageLinkCtrl">',
                        'after'           => '</div>',
                        'link_before'     => ' <h2><span>',
                            'link_after'      => '</span><span class="theLine"></span></h2>'
                            )) ?>
                            <!--<ul class="containerWider centeredBox dark" data-ctrl="nav">
                                    
                                <li class="pageLinkBox horizontal" data-linkbox data-for="Story">
                                    <div class="pageLinkCtrl">
                                        <a href="story.html" class="pageLinkTitle" data-ctrl="expand" data-async>
                                            <h2>
                                            <span>Our story</span>
                                            <span class="theLine"></span>
                                            </h2>
                                        </a>
                                    </div>
                                </li>
                            </ul>-->
                            <footer class="siteFooter" data-ctrl="nav">
                                <nav class="secondaryNav">
                                    <ul class="footerLinkGroup shopLinks">
                                        <li data-for="Shop"><a href="<?php echo site_url() ?>/search" class="siteBtn outlinedBtn leftIconBtn" data-async><div><span class="icon icon-lupa"></span>Search</div></a></li>
                                    </ul>
                                    <div class="linksGroupWrapper">
                                        <?php wp_nav_menu( array( 'menu' => 'sub-menu' , 'menu_class' => 'footerLinkGroup secondaryLinks', 'menu_id' => 'sub-menu' ) ); ?>
                                    </div>
                                </nav>
                                <div class="disclamerTxt">
                                    <p><?php $examplePost = get_post(32);
                                    echo $examplePost->post_content;  ?></p>
                                    <p>Design by &nbsp; <a href="http://www.north2.net" target="_blank"> north2</a></p>
                                </div>
                            </footer>
                        </nav>
                        <!-- Podnavigacija - proizvodi -->
                        <!-- Testing-class: privremeno da se vidi kako izgleda bez pageShortTexta na sredini -->
                        <section class="subNavSection subNav" data-subnav-slides data-subnav>
                            <!-- Intro slide -->
                            <div class="fullSpaceSection prodCatSection prodCatSection-0 active" data-slide>
                                <div class="bgImageContainer dark"></div>
                            </div>
                            <!-- First slide -->
                            <div class="fullSpaceSection prodCatSection prodCatSection-1" data-slide>
                                <div class="bgImageContainer dark"></div>
                                <div class="containerNarrower centeredBox">
                                    <p class="pageShortText"><!-- Juices --></p>
                                </div>
                            </div>
                            <!-- Second slide -->
                            <div class="fullSpaceSection prodCatSection prodCatSection-2" data-slide>
                                <div class="bgImageContainer dark"></div>
                                <div class="containerNarrower centeredBox">
                                    <p class="pageShortText"><!-- Jams --></p>
                                </div>
                            </div>
                            <!-- Third slide -->
                            <div class="fullSpaceSection prodCatSection prodCatSection-3" data-slide>
                                <div class="bgImageContainer dark"></div>
                                <div class="containerNarrower centeredBox">
                                    <p class="pageShortText"><!-- Ajvar --></p>
                                </div>
                            </div>
                            <!-- Links for product categories -->
                            <div class="pageLinksContainer containerNarrower centeredBox" data-onexit="fade-out">
                                <div class="pageLinkBox" data-linkbox>
                                    <div class="pageLinkCtrl">
                                        <a href="juices/index.html" class="pageLinkTitle" data-ctrl="expand change-slide" data-target="1" data-async data-quiet="true">
                                            <h2>Juices</h2>
                                            <span class="theLine"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="pageLinkBox" data-linkbox>
                                    <div class="pageLinkCtrl">
                                        <a class="pageLinkTitle" href="jams/index.html" data-ctrl="expand change-slide" data-target="2" data-async data-quiet="true">
                                            <h2>Jams</h2>
                                            <span class="theLine"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="pageLinkBox" data-linkbox>
                                    <div class="pageLinkCtrl">
                                        <a href="ajvar/index.html" class="pageLinkTitle" data-ctrl="expand change-slide" data-target="3" data-async data-quiet="true">
                                            <h2>Ajvar</h2>
                                            <span class="theLine"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="loadingSprite">
                                <div class="anim" data-decoration data-preload>
                                    <img src="web-assets/img/design/gs_loader.png" alt="Loading...">
                                </div>
                            </div>
                        </section>
                        <div class="pageWrapper">
                            <div class="asyncWrapper">
                                <!-- batas header  tetek bengek -->