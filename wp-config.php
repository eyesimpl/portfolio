<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ey_portfolio');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CnQ(vu-J;.bo1Y_0{0IEV-n^t.%Na6JA^pi|K|P)|06;=/uYhzAYHcm><TY03`d ');
define('SECURE_AUTH_KEY',  '`ueP7~bXbr|F,I&oa>C/!4B_@-G>Vacx@B#::^#T#ZSof=_iKC+77Dbg4OGq3NpC');
define('LOGGED_IN_KEY',    '_$:ndRdg@8UZ~O4XHt*zd(3<ai-G-2lw(~M.VV{{M%_z|v<pbD6e$*j|vA^q0/8,');
define('NONCE_KEY',        '^n#p71!_&pm8yD|z3`li]NgPH?Un?i/OhjJRfheGCnwE0Uh`:|.xsv7dnf8ayLZJ');
define('AUTH_SALT',        '-m6JZ*)sPo 21$n|+AH;l,elUw3!n~k>)#wu7@o||BWb2AmwO,>7x,49LSK`|/ra');
define('SECURE_AUTH_SALT', '[Nxi9(^Qe]T}d0veF[-h~uFE(Xx:7vQpF`V+xM[&+L%3kL|SIn(vgo~4ap$9:N6;');
define('LOGGED_IN_SALT',   '53ceQy!CU8l6~+%x}YIF#d4O:=V+7|-tlsm*g,h7&6F$X;,eN2<fk7H_?a3(rlm%');
define('NONCE_SALT',       'R]%C.1fQphpt|xLrf{J1<OFGSyAxS&?d!4%A!]^Cp-VyU-WN@.7kKxY*k3%V?L}t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
